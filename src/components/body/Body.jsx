import { Component } from "react";
import BodyInput from "./body-input/BodyInput";
import BodyOutput from "./body-output/BodyOutput";

class Body extends Component {
    constructor(props) {
        super(props);

        this.state = {
            inputMessage: "",
            outputMessage: [],
            likeDisplay: false
        }
    }

    // Tạo ra 1 hàm cho phép update state
    updateInputMessage = (message) => {
        this.setState({
            inputMessage: message
        })
    }

    updateOutputMessage = () => {
        if(this.state.inputMessage) {
            this.setState({
                outputMessage: [...this.state.outputMessage, this.state.inputMessage],
                likeDisplay: true
            })
        }
    }

    render() {
        return (
            <>
                <BodyInput 
                    inputMessageProp={this.state.inputMessage} 
                    updateInputMessageProp={this.updateInputMessage}
                    updateOutputMessageProp={this.updateOutputMessage}
                />

                <BodyOutput outputMessageProp={this.state.outputMessage} likeDisplayProp={this.state.outputMessage} />
            </>
        )
    }
}

export default Body;