import { Component } from "react";

import likeImage from "../../../assets/images/like.png";

class BodyOutput extends Component {
    render() {
        const { outputMessageProp, likeDisplayProp } = this.props;

        return (
            <>
                <div className="row mt-3 text-primary">
                    {
                        outputMessageProp.map(function(element, index) {
                            return (
                                <p key={index}>{element}</p>
                            );
                        })
                    }
                </div>
                <div className="row mt-3 mb-5">
                    {
                        likeDisplayProp ? <img alt="like" src={likeImage} style={{width: "100px", margin: "auto"}}/> : null
                    }
                    
                </div>
            </>
        )
    }
}

export default BodyOutput;